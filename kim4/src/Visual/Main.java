//메인화면
package Visual;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Main {
	private JFrame frame;
	private ImageIcon icon;
	private JLabel MainImage;
	private Clip clip;
	private GUIJDBCMember jdbcMember;
	private Ch_01 ch01;
	private Ch_02 ch02;
	private Ch_03 ch03;
	private Ch_04 ch04;

	public Main() {
		try {
			System.out.println("Main 생성자 호출됨");
			frame = new JFrame("자바 프로젝트");
			icon = new ImageIcon("D:\\20171171 진채은\\kim4\\src\\Visual\\Main.png");
			MainImage = new JLabel(icon);
			jdbcMember = GUIJDBCMember.getInstance();
			jdbcMember.readFile(); //제일 처음 시작할때는 파일을 읽어옴
			System.out.println("파일 읽어온 후 : " + Jujang.stage);

			ch01 = new Ch_01(this);
			ch02 = new Ch_02(this);
			ch03 = new Ch_03(this);
			ch04 = new Ch_04(this);
			
			frame.setTitle("자바 프로젝트");
	
			frame.addKeyListener(new MyKeyListener());
			frame.requestFocus();
			frame.setUndecorated(true);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setBounds(50, 50, 1200, 800);
			frame.setShape(new java.awt.geom.RoundRectangle2D.Double(0, 0, 1200, 800, 125, 125));

			frame.add(MainImage);
			Main_Sound();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Ch_01 getCh01(){
		return ch01;
	}
	
	public Ch_02 getCh02(){
		return ch02;
	}
	
	public Ch_03 getCh03(){
		return ch03;
	}
	
	public Ch_04 getCh04(){
		return ch04;
	}
	
	public JFrame getFrame(){
		return frame;
	}
	
	public GUIJDBCMember getJdbcMember(){
		return jdbcMember;
	}

	class MyKeyListener extends KeyAdapter {
		public void keyReleased(KeyEvent e) {
			System.out.println("Jujang.stage = " + Jujang.stage);
			if (e.getKeyCode() == 32) // 스페이스바 새로 시작
			{
				ch01.Ch_01();

				Stop_Main_Sound();
				frame.setVisible(false);
			}

			else if (e.getKeyCode() == 27) {// esc 종료
				System.exit(0);
			} else if (e.getKeyCode() == 40){ // 아래 키 눌렀을 때 이어하기
				System.out.println("아래 키 눌렀을 때");
				if (Jujang.stage == 1) { // DB의 number값이 1일때
					ch02.Ch_02();
					Stop_Main_Sound();
					frame.setVisible(false);
				} else if (Jujang.stage == 2) { // DB의 number값이 2일때
					ch03.Ch_03();

					Stop_Main_Sound();
					frame.setVisible(false);
				} else if (Jujang.stage == 3) { // DB의 number값이 3일때
					ch04.Ch_04();

					Stop_Main_Sound();
					frame.setVisible(false);
				}

			}
		}
	}

	public void Main_Sound() {
		AudioInputStream ais = null;
		BufferedInputStream bis = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("D:\\20171171 진채은\\kim4\\src\\Visual\\javamusic.wav");
			bis = new BufferedInputStream(fis);
			ais = AudioSystem.getAudioInputStream(bis);
			clip = AudioSystem.getClip();
			clip.stop();
			clip.close();
			clip.open(ais);
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ais.close();
				bis.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void Stop_Main_Sound() {
		try {
			clip.stop();
			clip.close();
		} catch (Exception e) {
		}
	}

	public static void main(String[] args) {
		new Main();
	}

}