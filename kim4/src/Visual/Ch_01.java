//Ch_01 진행화면
package Visual;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Ch_01 {
   // int stage1=2;
   // int storage=stage1; //storage 변수를 여러 파일에서도 사용 가능하게 해야함

   private JFrame c01 = new JFrame();
   private ImageIcon icon = new ImageIcon("D:\\20171171 진채은\\kim4\\src\\Visual\\ch_01.png");
   private JLabel Ch1Image = new JLabel(icon);
   private Main main;
   private Clip clip;

   public Ch_01(Main main) {
      System.out.println("Ch_01 생성자 호출됨");
      this.main = main;
   }

   public void Ch_01() {
      // c01.addKeyListener(new Exit_Ch_01());
      // c01.addKeyListener(new Move_Ch_002());
      // c01.addKeyListener(new db_s1());
      c01.addKeyListener(new Ch01KeyListener(main));

      c01.setUndecorated(true);
      c01.setBounds(50, 50, 1200, 800);
      c01.setVisible(true);
      c01.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      c01.requestFocus();
      c01.setShape(new java.awt.geom.RoundRectangle2D.Double(0, 0, 1200, 800, 125, 125));

      c01.add(Ch1Image);
      Ch_01_Sound();
   }

   class Ch01KeyListener extends KeyAdapter {
      private Main main;

      public Ch01KeyListener(Main main) {
         this.main = main;
      }

      public void keyReleased(KeyEvent e) {
         if (e.getKeyCode() == KeyEvent.VK_UP)// 스페이스 32 윗 방향키 38
         // 위쪽 방향키 누르면 이동
         {
            Stop_Ch_01_Sound();
            Ch_01_2_Sound();
         }

         else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            Stop_Ch_01_Sound();
            Ch_01_1_Sound();
         }

         else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            Stop_Ch_01_Sound();
            Ch_01_2_2_Sound();
         }

         else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            Stop_Ch_01_Sound();
            Ch_01_2_1_Sound();
         }

         else if (e.getKeyCode() == 27) {// esc 종료
            System.exit(0);
         }

         else if (e.getKeyCode() == 49) { //// 1 누르면 GUIJDBCMember에 저장

            Jujang.stage = 1;
            main.getJdbcMember().writeFile(); // 이런식으로 호출하면 파일에 쓰여짐
            main.getJdbcMember().action();
            Stop_Ch_01_Sound();
            main.getCh02().Ch_02();
            main.getFrame().setVisible(false);

            // GUIJDBCMember.main(null);

         }
      }
   }

   public void Ch_01_Sound() {
      AudioInputStream ais = null;
      BufferedInputStream bis = null;
      FileInputStream fis = null;
      try {
         fis = new FileInputStream("D:\\20171171 진채은\\채은\\챕터1-선택지1전.wav");
         bis = new BufferedInputStream(fis);
         ais = AudioSystem.getAudioInputStream(bis);
         clip = AudioSystem.getClip();
         clip.stop();
         clip.close();
         clip.open(ais);
         clip.start();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            ais.close();
            bis.close();
            fis.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   public void Ch_01_1_Sound() {
      AudioInputStream ais = null;
      BufferedInputStream bis = null;
      FileInputStream fis = null;
      try {
         fis = new FileInputStream("D:\\20171171 진채은\\채은\\챕터1-선택지1.wav");
         bis = new BufferedInputStream(fis);
         ais = AudioSystem.getAudioInputStream(bis);
         clip = AudioSystem.getClip();
         clip.stop();
         clip.close();
         clip.open(ais);
         clip.start();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            ais.close();
            bis.close();
            fis.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   public void Ch_01_2_Sound() {
      AudioInputStream ais = null;
      BufferedInputStream bis = null;
      FileInputStream fis = null;
      try {
         fis = new FileInputStream("D:\\20171171 진채은\\채은\\챕터1-선택지2의 선택지1 전.wav");
         bis = new BufferedInputStream(fis);
         ais = AudioSystem.getAudioInputStream(bis);
         clip = AudioSystem.getClip();
         clip.stop();
         clip.close();
         clip.open(ais);
         clip.start();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            ais.close();
            bis.close();
            fis.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   public void Ch_01_2_1_Sound() {
      AudioInputStream ais = null;
      BufferedInputStream bis = null;
      FileInputStream fis = null;
      try {
         fis = new FileInputStream("D:\\20171171 진채은\\채은\\챕터1-선택지2의 선택지1.wav");
         bis = new BufferedInputStream(fis);
         ais = AudioSystem.getAudioInputStream(bis);
         clip = AudioSystem.getClip();
         clip.stop();
         clip.close();
         clip.open(ais);
         clip.start();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            ais.close();
            bis.close();
            fis.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   public void Ch_01_2_2_Sound() {
      AudioInputStream ais = null;
      BufferedInputStream bis = null;
      FileInputStream fis = null;
      try {
         fis = new FileInputStream("D:\\20171171 진채은\\채은\\챕터1-선택지2의 선택지2.wav");
         bis = new BufferedInputStream(fis);
         ais = AudioSystem.getAudioInputStream(bis);
         clip = AudioSystem.getClip();
         clip.stop();
         clip.close();
         clip.open(ais);
         clip.start();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            ais.close();
            bis.close();
            fis.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   public void Stop_Ch_01_Sound() {
      try {
         clip.stop();
         clip.close();
      } catch (Exception e) {
      }
   }
}
//D:\\20171171 진채은\\kim4\\src\\Visual\\ch_01.png
//D:\\20171171 진채은\\채은\\챕터1-선택지1.wav