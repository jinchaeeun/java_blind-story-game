//Ch_04 진행화면
package Visual;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Ch_04
{
	int stage4=4;
	int storage=stage4; //storage 변수를 여러 파일에서도 사용 가능하게 해야함
	
    static JFrame c04 = new JFrame();
    static ImageIcon icon = new ImageIcon("C:\\Users\\kbg05\\workspace\\kim\\src\\Visual\\ch_04.jpg");
    static JLabel Ch1Image = new JLabel(icon);
    private Main main;
    
    public Ch_04(Main main){
    	this.main = main;
    }
    
   public void Ch_04()
   {
//	   c04.addKeyListener(new Exit_Ch_04());
//	   c04.addKeyListener(new Move_Main());
//	   c04.addKeyListener(new db_s4());
	   c04.addKeyListener(new Ch04KeyListener(main));
	   c04.setUndecorated(true);
	   c04.setBounds(50, 50, 1200, 800);
	   c04.setVisible(true);
	   c04.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   c04.requestFocus();
	   c04.setShape(new java.awt.geom.RoundRectangle2D.Double(0, 0, 1200, 800, 125, 125));
     
	   c04.add(Ch1Image);
   }
}


class Ch04KeyListener extends KeyAdapter {
	private Main main;
	
	public Ch04KeyListener(Main main){
		this.main = main;
	}
   public void keyReleased(KeyEvent e) 
   {
     /* if (e.getKeyCode() == KeyEvent.VK_UP)//스페이스 32 윗 방향키 38  
    	  //위쪽 방향키 누르면 이동
      {
    	    new Main();

			Main.Stop_Main_Sound();
			Main.frame.setVisible(false);
      }
   
      else*/ if(e.getKeyCode() == 27) {//esc 종료
    	  System.exit(0);
      }
      
	   else if(e.getKeyCode() == 49){ ////1 누르면 GUIJDBCMember에 저장
		   Jujang.stage=4;
		   main.getJdbcMember().writeFile();
		   main.getJdbcMember().action();
		   //GUIJDBCMember.main(null);
 
	  }
   }
}

