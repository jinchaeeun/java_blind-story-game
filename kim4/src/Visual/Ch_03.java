//Ch_03 진행화면
package Visual;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Ch_03
{
	//int stage3=3;
	//int storage=stage3; //storage 변수를 여러 파일에서도 사용 가능하게 해야함
	
    static JFrame c03 = new JFrame();
    static ImageIcon icon = new ImageIcon("C:\\Users\\kbg05\\workspace\\kim\\src\\Visual\\ch_03.jpg");
    static JLabel Ch1Image = new JLabel(icon);
    private Main main;
    
    public Ch_03(Main main){
    	this.main = main;
    }
    
   public void Ch_03()
   {
//	   c03.addKeyListener(new Exit_Ch_03());
//	   c03.addKeyListener(new Move_Ch_004());
//	   c03.addKeyListener(new db_s3());
	   c03.addKeyListener(new Ch03KeyListener(main));
	   c03.setUndecorated(true);
	   c03.setBounds(50, 50, 1200, 800);
	   c03.setVisible(true);
	   c03.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   c03.requestFocus();
	   c03.setShape(new java.awt.geom.RoundRectangle2D.Double(0, 0, 1200, 800, 125, 125));
     
	   c03.add(Ch1Image);
   }
}


class Ch03KeyListener extends KeyAdapter{
	private Main main;
	
	public Ch03KeyListener(Main main){
		this.main = main;
	}
   public void keyReleased(KeyEvent e) 
   {
      if (e.getKeyCode() == KeyEvent.VK_UP)//스페이스 32 윗 방향키 38  
    	  //위쪽 방향키 누르면 이동
      {
    	    main.getCh04().Ch_04();

			main.Stop_Main_Sound();
			main.getFrame().setVisible(false);
      }
   
      else if(e.getKeyCode() == 27) {//esc 종료
    	  System.exit(0);
      }
      
	   else if(e.getKeyCode() == 49){ ////1 누르면 Main에 저장
		   Jujang.stage=3;
		   main.getJdbcMember().writeFile();
		   main.getJdbcMember().action();
		   //Main.main(null);
 
	  }
   }
}
