//Ch_02 진행화면
package Visual;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Ch_02 {
	// int stage2=2;

	static JFrame c02 = new JFrame();
	static ImageIcon icon = new ImageIcon("C:\\Users\\el072\\eclipse-workspace\\JAVA2\\src\\Visual\\ch_01.png");
	static JLabel Ch1Image = new JLabel(icon);
	private Main main;
	private Clip clip;

	public Ch_02(Main main) {
		this.main = main;
	}

	public void Ch_02() {
		// c02.addKeyListener(new Exit_Ch_02());
		// c02.addKeyListener(new Move_Ch_003());
		// c02.addKeyListener(new db_s2());
		c02.addKeyListener(new Ch02KeyListener(main));
		c02.setUndecorated(true);
		c02.setBounds(50, 50, 1200, 800);
		c02.setVisible(true);
		c02.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		c02.requestFocus();
		c02.setShape(new java.awt.geom.RoundRectangle2D.Double(0, 0, 1200, 800, 125, 125));

		c02.add(Ch1Image);
		Ch_02_Sound();
	}

	class Ch02KeyListener extends KeyAdapter {
		private Main main;

		public Ch02KeyListener(Main main) {
			this.main = main;
		}

		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_LEFT)// 스페이스 32 윗 방향키 38
			// 위쪽 방향키 누르면 이동
			{
				Stop_Ch_02_Sound();
				Ch_02_1_Sound();
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT)// 스페이스 32 윗 방향키 38
			// 위쪽 방향키 누르면 이동
			{
				Stop_Ch_02_Sound();
				Ch_02_2_Sound();
			}

			else if (e.getKeyCode() == 27) {// esc 종료
				System.exit(0);

			}

			else if (e.getKeyCode() == 50) { //// 1 누르면 Main에 저장

				Jujang.stage = 2;
				main.getJdbcMember().writeFile();
				main.getJdbcMember().action();
				Stop_Ch_02_Sound();
				main.getCh03().Ch_03();
				main.getFrame().setVisible(false);
				// Main.main(null);

			}
		}
	}

	public void Ch_02_Sound() {
		AudioInputStream ais = null;
		BufferedInputStream bis = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C:\\Users\\el072\\eclipse-workspace\\JAVA2\\src\\Visual\\챕터2-선택지1전.wav");
			bis = new BufferedInputStream(fis);
			ais = AudioSystem.getAudioInputStream(bis);
			clip = AudioSystem.getClip();
			clip.stop();
			clip.close();
			clip.open(ais);
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ais.close();
				bis.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void Ch_02_1_Sound() {
		AudioInputStream ais = null;
		BufferedInputStream bis = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("챕터 2-1가 들어와야됨");
			bis = new BufferedInputStream(fis);
			ais = AudioSystem.getAudioInputStream(bis);
			clip = AudioSystem.getClip();
			clip.stop();
			clip.close();
			clip.open(ais);
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ais.close();
				bis.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void Ch_02_2_Sound() {
		AudioInputStream ais = null;
		BufferedInputStream bis = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("챕터 2-2가 들어와야됨");
			bis = new BufferedInputStream(fis);
			ais = AudioSystem.getAudioInputStream(bis);
			clip = AudioSystem.getClip();
			clip.stop();
			clip.close();
			clip.open(ais);
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ais.close();
				bis.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void Stop_Ch_02_Sound() {
		try {
			clip.stop();
			clip.close();
		} catch (Exception e) {
		}
	}

}
